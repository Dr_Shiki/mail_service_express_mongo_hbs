const jwt = require('jsonwebtoken');


module.exports = (req,res,next)=>
{
    token = req.session.token;
    if(!token)
    {
        //res.status(401).json({message : 'There is no authHeader'});
        res.redirect('/signIn');
    }
    /*const authHeader = req.get('Authorization');
    if(!authHeader)
    {
        res.status(401).json({message : 'There is no authHeader'});
        res.redirect('/signIn');
    }
    const authToken = authHeader.replace('Bearer ','');*/
    else
    {
        try
        {
            jwt.verify(token,'Shiki');
        }
        catch (e)
        {
            if(e instanceof jwt.JsonWebTokenError)
            {
                res.status(401).json({message : 'Token incorrect'});
                res.redirect('/signIn');
            }
            res.redirect('/signIn');
        }
        next();
    }
}