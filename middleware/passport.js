const passport = require('passport');

const jwtStategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user')

const options = 
{
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'Shiki'
}

module.exports = passport =>
{
    passport.use
    (
        new jwtStategy(options,async (payload,done)=>
        {
            try
            {
                const user = await User.findById(payload.userId).select('login id')

                if(user)
                {
                    done(null,user);
                }
                else
                {
                    done(null,false)
                }
            }
            catch(e)
            {
                
            }
        })
    )
}