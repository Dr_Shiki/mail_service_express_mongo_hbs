const { Router } = require('express')
const controllers = require('../controllers/controllers')
const passport = require('passport');
const auth = require('../middleware/auth')
const router = Router()

router.get('/',auth,controllers.GetGoHome);

router.get('/signIn',controllers.GetSignIn);
router.post('/signIn',controllers.PostSignIn);

//router.get('/signUp',passport.authenticate('jwt',{session: null}),controllers.GetSignUp);
//router.post('/signUp',passport.authenticate('jwt',{session: null}),controllers.PostSignUp);
router.get('/signUp',controllers.GetSignUp);
router.post('/signUp',controllers.PostSignUp);

router.get('/sendMail',auth,controllers.GetSendMail)
router.post('/sendMail',auth,controllers.PostSendMail)

router.get('/logOut',controllers.GetLogOut)

router.post('/openMail',auth,controllers.PostOpenMail)

router.get('/imageBoard',auth,controllers.GetImageBoard)

router.get('/ftp',auth,(req,res)=>
{
    res.redirect('ftp://shiki:nintendo@192.168.137.1:21')
})
module.exports = router
