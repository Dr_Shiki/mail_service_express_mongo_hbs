const mongoose = require('mongoose')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const bCrypt = require('bcrypt');
const Todo = require('../models/Todo');
const User = require('../models/user');
const Mail = require('../models/mail');
const { session } = require('passport');


const PostSignIn = (req,res)=>
{
    const {email , password} = req.body;
    User.findOne({login: email}).exec().then((user)=>
    {
        if(!user)
        {
            req.session.signErr = 'Check your login or password';
            res.redirect('/signIn');
        }
        else
        {
            if(password==user.password)
            {
                const token = jwt.sign(user._id.toString(),'Shiki');
                req.session.token = token;
                req.session.userMail = email;
                req.session.save();
                req.session.signErr = null;
                res.redirect('/');
            }
            else
            {
                req.session.signErr = 'Check your login or password';
                res.redirect('/signIn');
            }
        }
    })
}

const PostSignUp = async (req,res)=>
{
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@mailer\.ru/;
    if(re.test(String(req.body.email).toLowerCase()))
    {
        var isExist = false;
        User.findOne({login: req.body.email.toLowerCase()}).exec().then(
            (inst)=>
            {
                if(!inst)
                {
                    isExist = false;
                }
                else
                {
                    isExist = true;
                }
            }
        )
        if(isExist)
        {
            req.session.emailErr = 'Email already exist';
            res.redirect('signUp');
        }
        else
        {
            const user = new User
            (
                {
                    login: req.body.email.toLowerCase(),
                    password: req.body.password
                }
            )
            try
            {
                await user.save();
                res.redirect('/signIn');
            }
            catch (e)
            {
                console.log('Can not be save');
            }
        }
    }
    else
    {
        req.session.emailErr = 'Email should be like example@mailer.ru';
        console.log('Email should be like example@mailer.ru');
        res.redirect('/signUp');
    }
}

const PostSendMail = async (req,res)=>
{
    const _sender = req.session.userMail;
    const newMail = new Mail
    (
        {
            sender: _sender,
            reciever: req.body.reciever,
            subject: req.body.subject,
            body: req.body.body
        }
    )
    await newMail.save();
    res.redirect('/');
}

const PostOpenMail = async (req,res)=>
{
    const MailWhole = await Mail.findById(req.body.id);
    res.render('mail', {
        title: MailWhole.subject,
        isIndex: true,
        sender: MailWhole.sender,
        subject: MailWhole.subject,
        body: MailWhole.body
      })
}

const GetSignIn = (req,res)=>
{
    res.render('signIn', {
        title: 'Create todo',
        isCreate: true,
        signErr: req.session.signErr
      })
}

const GetSignUp = (req,res)=>
{
    res.render('signUp', {
        title: 'Create todo',
        isCreate: true,
        err: req.session.emailErr
      })
}

const GetGoHome = (req,res)=>
{
    const userLogin = req.session.userMail;
    //Mail.watch().on('change',
    /*(data)=>
    {
        console.log(data);
        if(data.fullDocument.reciever==userLogin)
        {
            console.log('It is near to work')
            res.redirect('/');
        }
    })*/
    Mail.find({reciever: userLogin}).exec().then
    (
        (mails)=>
        {
        res.render('index', {
        title: 'Your mails',
        isIndex: true,
        mails
        })
    });
}
const GetSendMail = (req,res) =>
{
    res.render('sendMail', {
        title: 'Send Mail',
        isIndex: true
      })
}

const GetLogOut = (req,res)=>
{
    req.session.userMail = null;
    req.session.token = null;
    res.redirect('/signIn');
}

const GetImageBoard = (req,res)=>
{
    res.render('imageBoard', {
        title: 'Image Board'
      })
}

module.exports =
{
    PostSignIn,
    PostSignUp,
    PostSendMail,
    PostOpenMail,
    GetSignIn,
    GetSignUp,
    GetGoHome,
    GetSendMail,
    GetLogOut,
    GetImageBoard
}
