const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const exphbs = require('express-handlebars')
const todoRoutes = require('./routes/todos')
const passport = require('passport');
const session = require('express-session')

const PORT = process.env.PORT || 3000

const app = express()
const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(session({secret: 'Shiki',saveUninitialized:true}))
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))

app.use(todoRoutes)
app.use(passport.initialize());
require('./middleware/passport')(passport)

async function start() {
  try {
    await mongoose.connect(
      //'mongodb+srv://Shiki:nintendo@cluster0.5hc0y.mongodb.net/mailServ',
      'mongodb://localhost:27017/mailServ',
      {
        useNewUrlParser: true,
        useFindAndModify: false
      }
    )
    app.listen(80,'192.168.137.1', () => {
      console.log('Server has been started...')
    })
  } catch (e) {
    console.log(e)
  }
}

start()
